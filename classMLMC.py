import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split

class MLMC():
    """
    Class for Multilevel Monte Carlo Integration
    """
    def __init__(self,
                 regressor=None,
                 split_train_percent=50,
                 integration_boundries=[0,1],
                 dims=None,
                 verbose=True
                 ):
        self.regressor = Pipeline([('scaler', StandardScaler()), ('regressor', regressor)])
        self.split_train_percent = split_train_percent
        self.integration_boundries=integration_boundries
        self.dims=dims
        self.I_mc=None
        self.I_mlmc=None
        self.verbose = verbose
        self.integration_volume = (integration_boundries[1]-integration_boundries[0])**dims

        #if verbose:
             #print("Integration Volume is {}".format(self.integration_volume))
    
    def get_estimates(self, Xtrain, ytrain, Xtest):
        '''
        Computes the two level estimation of the Integral (=Mean)
        '''
        self.Xtest = Xtest
        self.Xtrain = Xtrain
        self.ytrain = ytrain
        self.N = len(self.ytrain)

        assert self.Xtrain.shape[1] == self.Xtest.shape[1] and self.Xtrain.shape[0] == self.ytrain.shape[0]

        self.get_mc_estimates()

        self.get_mlmc_estimates()

        return self.I_mc, self.I_mlmc
        
    def get_mc_estimates(self):
        '''
        Computes the Monte Carlo estimates of the Integral
        '''
        self.I_mc = self.ytrain.mean() * self.integration_volume
        return 
    
    def get_mlmc_estimates(self):
        '''
        Computes the ML Monte Carlo estimates of the Integral
        '''
        Xtr, Xpred, ytr, ytrue = train_test_split(self.Xtrain, self.ytrain,
                                                  train_size= self.split_train_percent/100,
                                                  random_state=43)
        #if self.verbose:
                #print('Splitting Xtr into', Xtr.shape[0], 'training samples, and', Xpred.shape[0], 'validation samples.')
        
        self.regressor.fit(Xtr,ytr)

        ytest = self.regressor.predict(self.Xtest)
        ypred = self.regressor.predict(Xpred)

        self.I_mlmc = (ytest.mean() + (ytrue - ypred).mean()) * self.integration_volume
        return