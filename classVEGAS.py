import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

class Vegas():
    '''
    This class implements the integration algorithm described in 
    G Peter Lepage. A new algorithm for adaptive multidimensional integration. Journal of Computational Physics, 27(2):192–203, 1978.12
    =============================================
    The initialisation takes the following arguments:

    -integrand: A function that takes an input of size (dims) and output a number
    -dims: dimension of the input
    -intervals: line segments of the hypercubes in each dimension. E.g. dims=3, intervals=3 -> 9 total hypercubes
    -samples: # samples per iteration of the algorithm, note: not all hypercubes are samples during each iteration
    -iterations: # iterations
    -K, alpha: constants used during grid update
    '''
    def __init__(self,integrand,dims,intervals,samples,
                 iterations,K=100,alpha=1.5,verbose=True):
        
        self.integrand = integrand
        self.dims = dims
        self.intervals = intervals # intervals/dimension = gridpoints - 1
        self.samples = samples # samples/iteration
        self.iterations = iterations
        self.K = K
        self.alpha = alpha
        self.verbose = verbose

        self.integrals = np.zeros(iterations) # integral value at each iteration
        self.variances = np.zeros(iterations) # variance value at each iteration
        self.grids = np.zeros((iterations,dims,intervals+1)) # grid configurations at each iteration

        self.cumulative_integral = np.zeros(iterations) # cumulative estimate of the integral after each iteration step
        self.cumulative_variance = np.zeros(iterations) # cumulative estimate of the variance after each iteration step
        self.chi_2 = 0 

    def get_estimate(self):
        '''
        This function implements the Vegas algorithm
        The grid points at iteration are saved in self.grids
        The integral and variance estimates are saved in self.integrals, self.variance, respectively
        The cumulative estimates of the integal and variance at each iteration are saved in self.cumulative_integral, self.cumulative_variance, respectively
        '''

        grid, fill = np.meshgrid(np.linspace(0,1,self.intervals+1), np.linspace(0,1,self.dims))
        
        # Iteration Loop
        for iteration in range(self.iterations):
            # Save current grid
            self.grids[iteration] = grid

            # Calculate grid spacings & sampling probabilites
            deltas = np.diff(grid)
            probs = 1/(self.intervals*deltas)

            # Refinement weightings |f_i(x)| & Integral running values
            avg_f = np.zeros((self.dims,self.intervals))
            S1 = 0
            S2 = 0

            # Sampling Loop
            for sample in range(self.samples):

                # Produce Sample
                cell = np.random.choice(self.intervals, size=self.dims)
                sample = np.zeros(self.dims)
                for dim in range(self.dims):
                    sample[dim] = np.random.uniform(low=grid[dim,cell[dim]], high=grid[dim,cell[dim]+1])
                val = self.integrand(sample)

                # Update running statistics
                tot_prob = 1
                for dim in range(self.dims):
                    tot_prob *= probs[dim,cell[dim]]
                S1 += val/tot_prob
                S2 += (val/tot_prob)**2
                for dim in range(self.dims):
                    avg_f[dim, cell[dim]] += (val*probs[dim,cell[dim]])**2/(tot_prob**2)
                avg_f_new = np.sqrt(avg_f) 
            
            # Compute/save estimates for integral & variance
            S1 /= self.samples
            S2 /= self.samples
            self.integrals[iteration] = S1
            self.variances[iteration] = (S2-S1**2)/(self.samples-1)

            if iteration==0:
                self.cumulative_integral[iteration] = S1
                self.cumulative_variance[iteration] = (S2-S1**2)/(self.samples-1)
            
            else:
                self.cumulative_integral[iteration]=np.sum(self.integrals[0:iteration+1]**3/self.variances[0:iteration+1]**2)/np.sum(self.integrals[0:iteration+1]**2/self.variances[0:iteration+1]**2)
                self.cumulative_variance[iteration]=self.cumulative_integral[iteration]/np.sqrt(np.sum(self.integrals[0:iteration+1]**2/self.variances[0:iteration+1]**2))
            
            # Grid Update
            ratios = avg_f_new*deltas + np.finfo(float).eps
        
            for dim in range(self.dims):
                ratios[dim] /= np.sum(ratios[dim])
            m = np.ceil(self.K*((ratios-1)/np.log(ratios))**self.alpha).astype(int)
            for dim in range(self.dims):
                refined_grid = np.r_[[0],np.cumsum(np.repeat(deltas[dim]/m[dim], m[dim]))]
                choices = np.round(np.linspace(0, len(refined_grid)-1, num=self.intervals+1)).astype(int)
                grid[dim] = refined_grid[choices]
        
        return

    def table(self, true_i):
        '''
        This function takes the true value of the integral and generates a table containing the estimates after each iteration
        '''
        data = np.column_stack((np.arange(self.iterations)+1,
                                self.integrals,
                                self.variances,
                                np.sqrt(self.variances),
                                np.abs(self.integrals-true_i)))
        self.chi_2 = np.sum((self.integrals-self.cumulative_integral[-1])**2/self.variances)

        # Tabulating
        if self.verbose: 
            print("DIMS =", self.dims)
            print("Intervals per dim =", self.intervals)
            print("Samples per it =", self.samples)
            print("iterations =", self.iterations)
            print("-" * 125)
            headers = ["iteration", "est. int.", "est. variance","est. std.", "|est. int. - true int.|"]
            print("{:<20} {:<25} {:<25} {:<25} {:<25}".format(headers[0], headers[1], headers[2], headers[3], headers[4]))
            print("-" * 125)
            for row in data:
                print("{:<20} {:<25} {:<25} {:<25} {:<25}".format(row[0].astype(int), row[1], row[2], row[3], row[4]))
            print("-" * 125)
            print("{:<40} {}".format("True value of I", true_i))
            print("{:<40} {}".format("Cumulative est. value of I", self.cumulative_integral[-1]))
            print("{:<40} {} {} {} {}".format("CHI2",self.chi_2,"(>",self.iterations-1,"?)"))
        return
    

    
