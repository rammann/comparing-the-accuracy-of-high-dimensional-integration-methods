# Comparing the Accuracy of High-Dimensional Integration Methods

This repository contains the python code written for my semesterthesis.

classVegas.py contains an implementation of the Vegas integration algorithm described in 
"G Peter Lepage. A new algorithm for adaptive multidimensional integration. Journal of Computational Physics, 27(2):192–203, 1978.12"

classMLMC.py contains a simple implementation of the two-level Monte Carlo estimator 

MLMC_plots.ipynb contains the code which was run to compare the performance of the methods and generate plots.




